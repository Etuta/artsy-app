//
//  ContentView.swift
//  Artsy App
//
//  Created by Veronika Hrbáčová on 01/05/2021.
//  Copyright © 2021 Veronika Hrbacova. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
